class_name WebSocketServer
extends Node



var server: TCPServer = TCPServer.new()
#var connected_peers: Array[WebSocketPeerHandler]
var incoming_peers: Array[WebSocketServerIncomingPeer]
var key: CryptoKey = CryptoKey.new()
var cert: X509Certificate = X509Certificate.new()
var crypto: Crypto = Crypto.new()

func get_cert_time() -> String:
	var ut: float = Time.get_unix_time_from_system() - 60.0 * 60.0 * 24.0
	var time: String = Time.get_date_string_from_unix_time(int(ut))
	for r in "-:T":
		time = time.replace(r, "")
	return time + "000000"


func _ready() -> void:
	print("original ready")

	if not ResourceLoader.exists("rsa.key") or not ResourceLoader.exists("self.crt"):
		print("create key and certificate")

		key = crypto.generate_rsa(4096)
		cert = crypto.generate_self_signed_certificate(key, "CN=localhost,O=wsg,C=DE", get_cert_time())
		key.save("rsa.key")
		cert.save("self.crt")
	else:
		print("load key and certificate")
		key.load("rsa.key")
		cert.load("self.crt")
	prints("Server starting:", server.listen(42069))


func _process(_delta: float) -> void:
	while server.is_connection_available():
		incoming_peers.append(
			WebSocketServerIncomingPeer.new(
				server.take_connection(),
				key, cert))
	
	for peer in incoming_peers.duplicate():
		match peer.poll():
			WebSocketServerIncomingPeer.STATUS.FAILED:
				incoming_peers.erase(peer)
			WebSocketServerIncomingPeer.STATUS.SUCCESS:
				incoming_peers.erase(peer)
				var ws_peer: WebSocketPeerHandler = WebSocketPeerHandler.new()
				add_child(ws_peer)
				#connected_peers.append(ws_peer)
				#ws_peer.disconnected.connect(connected_peers.erase.bind(ws_peed))
				ws_peer.disconnected.connect(ws_peer.queue_free.unbind(2))
				ws_peer.peer = peer.websocket
				ws_peer.data_received.connect(_data.bind(ws_peer.peer))
	


func _data(_packet_data: PackedByteArray, _was_string: bool, _peer: WebSocketPeer) -> void:
	pass
#	if not was_string:
#		return
#	# ab hier kommt nurnoch mist :D bitte ausbessern
#	var parts: PackedStringArray = packet_data.get_string_from_utf8().split(" ")
#	if parts.size() != 4:
#		peer.send_text("error arguments")
#		return
#	var module: String = parts[0]
#	var method: String = parts[1]
#	var username: String = parts[2]
#	var password_or_token: String = parts[3]
#
#	if not module == "user":
#		peer.close(1000, "module")
#		return
#	if not method in ["login", "register", "logout"]:
#		peer.close(1000, "method")
#		return
#	if not username.length() >= 3:
#		peer.close(1000, "username")
#		return
#	if not password_or_token.length() == 64:
#		peer.close(1000, "password")
#		return
#	var userdata: Dictionary
#	match method:
#		"login":
#			if not username in database:
#				peer.close(1000, "user_not_found")
#				return
#			userdata = database[username]
#			if not userdata["password"] == password_or_token:
#				peer.close(1000, "incorrect_password")
#				return
#
#		"register":
#			if username in database:
#				peer.close(1000, "user_exists")
#				return
#			userdata = USER_DATASET.duplicate()
#			userdata["password"] = password_or_token
#			userdata["registration"] = Time.get_datetime_string_from_system(true, true)
#			userdata["last_login"] = userdata["registration"]
#			database[username] = userdata
#
#		"logout":
#			if not username in database:
#				peer.close(1000, "user_not_found")
#				return
#			userdata = database[username]
#			if not password_or_token == userdata["token"]:
#				peer.close(1000, "token")
#				return
#			userdata["token"] = ""
#			peer.close(1000, "logout")
#			return
#
#	userdata["token"] = Marshalls.raw_to_base64(crypto.generate_random_bytes(64)).sha256_text()
#	peer.send_text("success %s %s" % [userdata["last_login"], userdata["token"]])
#	userdata["last_login"] = Time.get_datetime_string_from_system(true, true)
#
#
#
