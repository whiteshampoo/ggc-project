extends CMDHandler


var thread: Thread = Thread.new()


func _ready() -> void:
	add_text(CMD.welcome, "")
	CMD.execution_finished.connect(start_thread)
	start_thread()


func start_thread() -> void:
	if not CMD.shutdown:
		thread.start(stdin)


func stdin() -> void:
	printraw(" > ")
	command.call_deferred(OS.read_string_from_stdin().strip_escapes())


func command(text: String) -> void:
	thread.wait_to_finish()
	CMD.command(text)
	await CMD.command

