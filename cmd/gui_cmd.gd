extends CMDHandler


@export var max_lines: int = 1000


@onready var console: RichTextLabel = $ConsoleContainer/Console
@onready var input: LineEdit = $ConsoleContainer/InputContainer/Input


var lines: int = 0


func _ready() -> void:
	add_text(CMD.welcome, "")
	lines += CMD.welcome.count("\n") + 1


func _add_text(text: String, new_line: String) -> void:
	for line in text.split("\n"):
		lines += 1
		console.text += new_line + line
	while lines > max_lines:
		lines -= 1
		console.text = console.text.split("\n", false, 1)[1]


func command(cmd: String) -> void:
	cmd = cmd.strip_edges().strip_escapes()
	if cmd == "":
		return
	input.text = ""
	add_text(" > %s" % cmd)
	CMD.command(cmd)


func _on_input_text_submitted(new_text: String) -> void:
	command(new_text)


func _on_submit_pressed() -> void:
	command(input.text)
