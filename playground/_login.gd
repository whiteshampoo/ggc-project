extends VBoxContainer

const FONT_COLOR: String = "theme_override_colors/font_color"
const MINIMUM_PASSWORD_LENGTH: int = 6
const MINIMUM_USERNAME_LENGTH: int = 3

enum MODE {
	LOGIN,
	REGISTER,
}

@onready var title: Label = $Title
@onready var username: LineEdit = $Username
@onready var password: LineEdit = $Password
@onready var password_check: LineEdit = $PasswordCheck
@onready var login: Button = $Login
@onready var register: Button = $Register
@onready var client: WebSocketPeerHandler = $Client
@onready var logout: Button = $Logout

var mode: MODE = MODE.LOGIN
var token: String = ""
var registration: String = ""
var last_login: String = ""

func _ready() -> void:
	set_process(false)
	check_login()


func is_password_ok() -> bool:
	return password.text.length() >= MINIMUM_PASSWORD_LENGTH and not " " in password.text


func is_username_ok() -> bool:
	return username.text.length() >= MINIMUM_USERNAME_LENGTH and not " " in username.text


func check_login(_new_text: String = "") -> bool:
	if is_username_ok():
		username.set(FONT_COLOR, Color.WHITE)
	else:
		username.set(FONT_COLOR, Color.RED)
		login.disabled = true
	
	
		
	if is_password_ok():
		password.set(FONT_COLOR, Color.WHITE)
		password_check.set(FONT_COLOR, Color.WHITE)
	else:
		password.set(FONT_COLOR, Color.RED)
		password_check.set(FONT_COLOR, Color.RED)
		login.disabled = true
		
	if not is_username_ok() or not is_password_ok():
		return false
		
	match mode:
		MODE.LOGIN:
			login.disabled = false
		MODE.REGISTER:
			if not password.text == password_check.text:
				password.set(FONT_COLOR, Color.RED)
				password_check.set(FONT_COLOR, Color.RED)
				login.disabled = true
			else:
				login.disabled = false

	return not login.disabled


func _on_register_toggled(button_pressed: bool) -> void:
	mode = MODE.REGISTER if button_pressed else MODE.LOGIN
	check_login()


func all_xable(dis: bool) -> void:
	password.editable = not dis
	password_check.editable = not dis
	username.editable = not dis
	login.disabled = dis
	register.disabled = dis
	if not dis:
		check_login()
	

func _on_login_pressed() -> void:
	if not check_login():
		return

	client.peer = WebSocketPeer.new()
	prints("client connecting:", client.peer.connect_to_url("wss://127.0.0.1:42069", TLSOptions.client_unsafe()))
	client.data_received.connect(_data)
	client.connected.connect(connected, CONNECT_ONE_SHOT)
	client.disconnected.connect(disconnected, CONNECT_ONE_SHOT)
	all_xable(true)


func connected() -> void:
	client.peer.send(LoginData.login(username.text, password.text.sha256_text()))


func _data(packet_data: PackedByteArray, was_string: bool) -> void:
	if was_string:
		return
	if packet_data == LoginData.ERROR:
		print("Error")
		return
	var data: Dictionary = bytes_to_var(packet_data)
#	var parts: PackedStringArray = packet_data.get_string_from_utf8().split(" ")
#	if parts.size() == 0:
#		return
#	match parts[0]:
#		"success":
#			parts.remove_at(0)
#			title.text = " ".join(parts)
#			token = parts[-1]
#			client.peer.close()
#			logout.show()
#			register.hide()
#			login.hide()
#			username.hide()
#			password.hide()
#			password_check.hide()
#			return
#		_:
#			print("error :(")
#			client.peer.close()
#	all_xable(false)


func disconnected(code: int, reason: String) -> void:
	prints(code, reason)
	client.data_received.disconnect(_data)
	all_xable(false)


func _on_logout_pressed() -> void:
	var prompt: PackedStringArray = PackedStringArray()
	prompt.append("user")
	prompt.append("logout")
	prompt.append(username.text)
	prompt.append(token)

	client.peer = WebSocketPeer.new()
	prints("client connecting:", client.peer.connect_to_url("wss://127.0.0.1:42069", TLSOptions.client_unsafe()))
	client.connected.connect(client.peer.send_text.bind(" ".join(prompt)), CONNECT_ONE_SHOT)
	client.disconnected.connect(disconnected, CONNECT_ONE_SHOT)
	client.data_received.connect(_data)
	
	logout.hide()
	register.show()
	login.show()
	username.show()
	password.show()
	password_check.show()
