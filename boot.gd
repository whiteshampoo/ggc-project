extends Node

@export_file("*.tscn") var gui_cmd: String = "res://cmd/gui_cmd.tscn"
@export_file("*.tscn") var stdio_cmd: String = "res://cmd/stdio_cmd.tscn"

func _ready() -> void:

	if OS.has_feature("server") or "--server" in OS.get_cmdline_args():
		get_tree().change_scene_to_file(stdio_cmd)
		return
		
	get_tree().change_scene_to_file(gui_cmd)
