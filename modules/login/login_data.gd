class_name LoginData
extends Resource

static var minimum_username_length: int = 3
static var allowed_username_characters: String = """
abcdefghijklmnopqrstuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
0123456789_-
"""

enum DATA {
	TYPE,
	USER,
	NEW_USER,
	PASS,
	NEW_PASS,
	TOKEN,
	RESPONSE,
	REGISTRATION,
	LAST_LOGIN,
	MESSAGE,
}


enum TYPE {
	REGISTER,
	LOGIN,
	LOGOUT,
	UPDATE_USERNAME,
	UPDATE_PASSWORD,
	DELETE,
	RESPONSE,
	MAX,
}


enum RESPONSE {
	REGISTER_SUCESSFUL,
	LOGIN_SUCESSFUL,
	LOGOUT_SUCESSFUL,
	UPDATE_USER_SUCESSFUL,
	UPDATE_PASS_SUCESSFUL,
	DELETE_SUCESSFUL,
	USER_EXISTS,
	USER_NOT_EXISTS,
	USER_INVALID,
	PASSWORD_WRONG,
	PASSWORD_INVALID,
	TOKEN_WRONG,
	TOKEN_INVALID,
	REGISTER_DEACTIVATED,
	LOGIN_DEACTIVATED,
	USER_RENAME_NOT_ALLOWED,
	PACKET_INVALID,
	OTHER,
	MAX,
}

const ERROR: PackedByteArray = [0]

static func username_is_valid(username: String) -> bool:
	if username.length() < minimum_username_length:
		return false
	for c in username:
		if not c in allowed_username_characters:
			return false
	return true


static func usernames_are_valid(usernames: PackedStringArray) -> bool:
	for username in usernames:
		if not username_is_valid(username):
			return false
	return true


static func length_is_valid(string: String, length: int = 64) -> bool:
	return string.length() == length


static func lengths_are_valid(strings: PackedStringArray, length: int = 64) -> bool:
	for string in strings:
		if not length_is_valid(string, length):
			return false
	return true


static func check_data(data: Dictionary) -> RESPONSE:
	if not data or not DATA.TYPE in data:
		return RESPONSE.PACKET_INVALID
	
	if DATA.USER in data \
	and data[DATA.USER] is String \
	and not username_is_valid(data[DATA.USER]):
		return RESPONSE.USER_INVALID
		
	if DATA.NEW_USER in data \
	and data[DATA.NEW_USER] is String \
	and not username_is_valid(data[DATA.NEW_USER]):
		return RESPONSE.USER_INVALID
	
	if DATA.PASS in data \
	and data[DATA.PASS] is String \
	and not length_is_valid(data[DATA.PASS]):
		return RESPONSE.PASSWORD_INVALID
	
	if DATA.NEW_PASS in data \
	and data[DATA.NEW_PASS] is String \
	and not length_is_valid(data[DATA.NEW_PASS]):
		return RESPONSE.PASSWORD_INVALID
	
	if DATA.TOKEN in data \
	and data[DATA.TOKEN] is String \
	and not length_is_valid(data[DATA.TOKEN]):
		return RESPONSE.TOKEN_INVALID

	if DATA.RESPONSE in data \
	and data[DATA.RESPONSE] is RESPONSE \
	and not data[DATA.RESPONSE] in RESPONSE:
		return RESPONSE.PACKET_INVALID

	if DATA.REGISTRATION in data \
	and not data[DATA.REGISTRATION] is String:
		return RESPONSE.PACKET_INVALID

	if DATA.LAST_LOGIN in data \
	and not data[DATA.LAST_LOGIN] is String:
		return RESPONSE.PACKET_INVALID

	if DATA.MESSAGE in data \
	and not data[DATA.MESSAGE] is String:
		return RESPONSE.PACKET_INVALID
	
	return RESPONSE.MAX


static func register(username: String, password: String) -> PackedByteArray:
	if not username_is_valid(username) \
	or not length_is_valid(password):
		return ERROR
	return var_to_bytes({
		DATA.TYPE: TYPE.REGISTER,
		DATA.USER: username,
		DATA.PASS: password,
	})


static func login(username: String, password: String) -> PackedByteArray:
	if not username_is_valid(username) \
	or not length_is_valid(password):
		return ERROR
	return var_to_bytes({
		DATA.TYPE: TYPE.LOGIN,
		DATA.USER: username,
		DATA.PASS: password,
	})


static func logout(username: String, token: String) -> PackedByteArray:
	if not username_is_valid(username) \
	or not length_is_valid(token):
		return ERROR
	return var_to_bytes({
		DATA.TYPE: TYPE.LOGOUT,
		DATA.USER: username,
		DATA.TOKEN: token,
	})


static func update_username(username: String, new_username: String, password: String, token: String) -> PackedByteArray:
	if not usernames_are_valid([username, new_username]) \
	or not lengths_are_valid([password, token]):
		return ERROR
	return var_to_bytes({
		DATA.TYPE: TYPE.UPDATE_USERNAME,
		DATA.USER: username,
		DATA.NEW_USER: new_username,
		DATA.PASS: password,
		DATA.TOKEN: token,
	})


static func update_password(username: String, password: String, new_password: String, token: String) -> PackedByteArray:
	if not username_is_valid(username) \
	or not lengths_are_valid([password, new_password, token]):
		return ERROR
	return var_to_bytes({
		DATA.TYPE: TYPE.UPDATE_PASSWORD,
		DATA.USER: username,
		DATA.PASS: password,
		DATA.NEW_PASS: new_password,
		DATA.TOKEN: token,
	})


static func delete(username: String, password: String, token: String) -> PackedByteArray:
	if not username_is_valid(username) \
	or not lengths_are_valid([password, token]):
		return ERROR
	return var_to_bytes({
		DATA.TYPE: TYPE.DELETE,
		DATA.USER: username,
		DATA.PASS: password,
		DATA.TOKEN: token,
	})


static func response(_response: RESPONSE, extra: Dictionary = Dictionary()) -> PackedByteArray:
	if not _response is RESPONSE:
		return ERROR
	var data: Dictionary = {
		DATA.TYPE: TYPE.RESPONSE,
		DATA.RESPONSE: _response,
	}
	for x in extra:
		match x:
			DATA.TOKEN:
				if not extra[x] is String and not extra[x].length() == 64:
					return ERROR
				data[DATA.TOKEN] = extra[x]
			DATA.REGISTRATION:
				if not extra[x] is String and not extra[x].length() > 0:
					return ERROR
				data[DATA.REGISTRATION] = extra[x]
			DATA.LAST_LOGIN:
				if not extra[x] is String and not extra[x].length() > 0:
					return ERROR
				data[DATA.LAST_LOGIN] = extra[x]
			DATA.MESSAGE:
				if not extra[x] is String and not extra[x].length() > 0:
					return ERROR
				data[DATA.MESSAGE] = extra[x]
			_:
				return ERROR
	return var_to_bytes(data)
