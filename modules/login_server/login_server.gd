extends WebSocketServer

@export var database_filename: String = "res://database.json"
@export var send_registration: bool = true
@export var send_last_login: bool = true
@export var allow_registration: bool = true
@export var allow_login: bool = true
@export var allow_changing_username: bool = true

var database: Dictionary = Dictionary()
const USER_DATASET: Dictionary = {
	"password": "",
	"registration": "",
	"last_login": "",
	"token": "",
}


func _ready() -> void:
	super._ready()
	if FileAccess.file_exists(database_filename):
		database = JSON.parse_string(FileAccess.get_file_as_string(database_filename))


func get_login_dict(username: String) -> Dictionary:
	if not username in database:
		return Dictionary()
	var extra: Dictionary = {
		LoginData.DATA.TOKEN: database[username]["token"]
	}
	if send_registration:
		extra[LoginData.DATA.REGISTRATION] = database[username]["registration"]
	if send_last_login:
		extra[LoginData.DATA.LAST_LOGIN] = database[username]["last_login"]
	return extra


func _data(packet_data: PackedByteArray, was_string: bool, peer: WebSocketPeer) -> void:
	if was_string:
		peer.close()
		return
		
	var data: Dictionary = bytes_to_var(packet_data) as Dictionary
	if not LoginData.check_data(data) == LoginData.RESPONSE.MAX:
		peer.close()
		return
	
	match data[LoginData.DATA.TYPE]:
		LoginData.TYPE.REGISTER:
			if not LoginData.DATA.USER in data \
			or not LoginData.DATA.PASS in data:
				peer.close()
				return
			if not allow_registration:
				peer.send(LoginData.response(LoginData.RESPONSE.REGISTER_DEACTIVATED))
				return
			if data[LoginData.DATA.USER] in database:
				peer.send(LoginData.response(LoginData.RESPONSE.USER_EXISTS))
				return
			var dataset: Dictionary = USER_DATASET
			dataset["password"] = data[LoginData.DATA.PASS]
			dataset["registration"] = Time.get_datetime_string_from_system(true, true)
			dataset["last_login"] = dataset["registration"]
			dataset["token"] = Marshalls.raw_to_base64(crypto.generate_random_bytes(64)).sha256_text()
			database[data[LoginData.DATA.USER]] = dataset
			peer.send(LoginData.response(LoginData.RESPONSE.REGISTER_SUCESSFUL, get_login_dict(data[LoginData.DATA.USER])))
			return
		
		LoginData.TYPE.LOGIN:
			if not LoginData.DATA.USER in data \
			or not LoginData.DATA.PASS in data:
				peer.close()
				return
			if not allow_login:
				peer.send(LoginData.response(LoginData.RESPONSE.LOGIN_DEACTIVATED))
				return
			if not data[LoginData.DATA.USER] in database:
				peer.send(LoginData.response(LoginData.RESPONSE.USER_NOT_EXISTS))
				return
			var dataset: Dictionary = database[data[LoginData.DATA.USER]]
			if not dataset["password"] == data[LoginData.DATA.PASS]:
				peer.send(LoginData.response(LoginData.RESPONSE.PASSWORD_WRONG))
				return
			dataset["token"] = Marshalls.raw_to_base64(crypto.generate_random_bytes(64)).sha256_text()
			peer.send(LoginData.response(LoginData.RESPONSE.LOGIN_SUCESSFUL, get_login_dict(data[LoginData.DATA.USER])))
			dataset["last_login"] = Time.get_datetime_string_from_system(true, true)
			return
			
		LoginData.TYPE.LOGOUT:
			if not LoginData.DATA.USER in data \
			or not LoginData.DATA.TOKEN in data:
				peer.close()
				return
			if not data[LoginData.DATA.USER] in database:
				peer.send(LoginData.response(LoginData.RESPONSE.USER_NOT_EXISTS))
				return
			var dataset: Dictionary = database[data[LoginData.DATA.USER]]
			if not dataset["token"] == data[LoginData.DATA.TOKEN]:
				peer.send(LoginData.response(LoginData.RESPONSE.TOKEN_WRONG))
				return
			dataset["token"] = ""
			peer.send(LoginData.response(LoginData.RESPONSE.LOGOUT_SUCESSFUL))
			return
			
		LoginData.TYPE.UPDATE_USERNAME:
			if not LoginData.DATA.USER in data \
			or not LoginData.DATA.NEW_USER in data \
			or not LoginData.DATA.PASS in data \
			or not LoginData.DATA.TOKEN in data:
				peer.close()
				return
			if not allow_changing_username:
				peer.send(LoginData.response(LoginData.RESPONSE.USER_RENAME_NOT_ALLOWED))
				return
			if not data[LoginData.DATA.USER] in database:
				peer.send(LoginData.response(LoginData.RESPONSE.USER_NOT_EXISTS))
				return
			var dataset: Dictionary = database[data[LoginData.DATA.USER]]
			if not dataset["password"] == data[LoginData.DATA.PASS]:
				peer.send(LoginData.response(LoginData.RESPONSE.PASSWORD_WRONG))
				return
			if not dataset["token"] == data[LoginData.DATA.TOKEN]:
				peer.send(LoginData.response(LoginData.RESPONSE.TOKEN_WRONG))
				return
			database.erase(data[LoginData.DATA.USER])
			database[data[LoginData.DATA.NEW_USER]] = dataset
			peer.send(LoginData.response(LoginData.RESPONSE.UPDATE_USER_SUCESSFUL))
			return
		LoginData.TYPE.UPDATE_PASSWORD:
			if not LoginData.DATA.USER in data \
			or not LoginData.DATA.PASS in data \
			or not LoginData.DATA.NEW_PASS in data \
			or not LoginData.DATA.TOKEN in data:
				peer.close()
				return
			if not data[LoginData.DATA.USER] in database:
				peer.send(LoginData.response(LoginData.RESPONSE.USER_NOT_EXISTS))
				return
			var dataset: Dictionary = database[data[LoginData.DATA.USER]]
			if not dataset["password"] == data[LoginData.DATA.PASS]:
				peer.send(LoginData.response(LoginData.RESPONSE.PASSWORD_WRONG))
				return
			if not dataset["token"] == data[LoginData.DATA.TOKEN]:
				peer.send(LoginData.response(LoginData.RESPONSE.TOKEN_WRONG))
				return
			dataset["password"] = data[LoginData.DATA.PASS]
			dataset["token"] = Marshalls.raw_to_base64(crypto.generate_random_bytes(64)).sha256_text()
			peer.send(LoginData.response(LoginData.RESPONSE.UPDATE_PASS_SUCESSFUL, {LoginData.DATA.TOKEN: dataset["token"]}))
			return
		LoginData.TYPE.DELETE:
			if not LoginData.DATA.USER in data \
			or not LoginData.DATA.PASS in data \
			or not LoginData.DATA.TOKEN in data:
				peer.close()
				return
			if not data[LoginData.DATA.USER] in database:
				peer.send(LoginData.response(LoginData.RESPONSE.USER_NOT_EXISTS))
				return
			var dataset: Dictionary = database[data[LoginData.DATA.USER]]
			if not dataset["password"] == data[LoginData.DATA.PASS]:
				peer.send(LoginData.response(LoginData.RESPONSE.PASSWORD_WRONG))
				return
			if not dataset["token"] == data[LoginData.DATA.TOKEN]:
				peer.send(LoginData.response(LoginData.RESPONSE.TOKEN_WRONG))
				return
			database.erase(data[LoginData.DATA.USER])
	peer.close() # <- Fail


func _on_tree_exiting() -> void:
	var file: FileAccess = FileAccess.open(database_filename, FileAccess.WRITE)
	file.store_string(JSON.stringify(database, "    ", true))
