extends LoginClientDemoTemplate

@export var register_scene: String = "res://modules/login_client_demo/register.tscn"
@export var main_menu_scene: String = "res://modules/login_client_demo/main.tscn"

@onready var server: LineEdit = $Server
@onready var username: LineEdit = $Username
@onready var password: LineEdit = $Password

func _ready() -> void:
	client.data_received.connect(_data)

func _on_login_pressed() -> void:
	client.peer = WebSocketPeer.new()
	var error: Error = client.peer.connect_to_url(server.text, TLSOptions.client_unsafe())
	if not error == OK:
		info.text = "Unable to connect to server (%d)" % error
		client.peer = null
		return
	client.connected.connect(connected, CONNECT_ONE_SHOT)
	client.disconnected.connect(disconnected, CONNECT_ONE_SHOT)


func connected() -> void:
	print("connected")
	var packet: PackedByteArray = LoginData.login(username.text, password.text.sha256_text())
	if packet == LoginData.ERROR:
		client.peer.close()
		info.text = "Error while sending login"
		return
	client.peer.send(packet)


func disconnected(code: int, reason: String) -> void:
	prints("disconnected", code, reason)


func _data(packet_data: PackedByteArray, was_string: bool) -> void:
	if was_string:
		return
	if packet_data == LoginData.ERROR:
		print("Error")
		return
	var data: Dictionary = bytes_to_var(packet_data)
	if not LoginData.check_data(data):
		info.text = "Invalid packet received"
		return
	if not data[LoginData.DATA.TYPE] == LoginData.TYPE.RESPONSE:
		info.text = "Invalid packet received"
		return
	match data[LoginData.DATA.RESPONSE]:
		LoginData.RESPONSE.LOGIN_SUCESSFUL:
			window.username = username.text
			window.token = data[LoginData.DATA.TOKEN]
			window.registration = data.get(LoginData.DATA.REGISTRATION, "unknown")
			window.last_login = data.get(LoginData.DATA.LAST_LOGIN, "unknown")
			window.add_child(load(main_menu_scene).instantiate())
			hide()
			queue_free()
			return
		LoginData.RESPONSE.USER_NOT_EXISTS:
			info.text = "User does not exist"
			return
		LoginData.RESPONSE.PASSWORD_WRONG:
			info.text = "Wrong password"
			return
		_:
			info.text = "Invalid response received"
			return

func _on_register_pressed() -> void:
	window.tmp_username = username.text
	window.tmp_password = password.text
	window.add_child(load(register_scene).instantiate())
	hide()
	queue_free()
