extends Window

var tmp_username: String = ""
var tmp_password: String = ""

var username: String = ""
var token: String = ""
var registration: String = ""
var last_login: String = ""

func exit() -> void:
	queue_free()
