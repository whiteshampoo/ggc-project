class_name LoginClientDemoTemplate
extends VBoxContainer

@export var back_scene: String = ""

@onready var client: WebSocketPeerHandler = $Client
@onready var info: Label = $Info
@onready var window: Window = get_window()


func _on_back_pressed() -> void:
	if not back_scene == "":
		get_parent().add_child(load(back_scene).instantiate())
		hide()
		queue_free()
		return
	window.quit()
